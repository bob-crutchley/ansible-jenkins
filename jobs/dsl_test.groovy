folder('flask-app')

job('flask-app/deploy') {
    scm {
        git('https://gitlab.com/bob-crutchley/ansible-jenkins')
    }
    steps {
        ansiblePlaybookBuilder {
            playbook('deploy.yaml') 
            inventory {
                inventoryPath {
                    path('swarm-manager,')
                }
            }
        }
    }
}

