#! /bin/bash
control_node=jenkins
instances=(
    manager
    worker
)
automation_user=jenkins
public_key=~/.ssh/jenkins_id_rsa.pub
gcloud compute instances create ${instances[@]} \
    --machine-type=f1-micro \
    --metadata startup-script="useradd -m -s /bin/bash ${automation_user} && mkdir -p /home/${automation_user}/.ssh && echo '$(cat ${public_key})' > /home/${automation_user}/.ssh/authorized_keys"

